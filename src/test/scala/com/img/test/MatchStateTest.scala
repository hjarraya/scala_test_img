package com.img.test

import org.junit.Test
import org.scalatest.Matchers
import org.scalatest.junit.AssertionsForJUnit

/**
 * Created by a-hjarraya on 6/22/15.
 */
class MatchStateTest extends AssertionsForJUnit with Matchers {

  @Test def testEventsOrder() = {
    val matchState = new MatchState()
    matchState.addEvent(new Event(Some(0), Some(0), Some(0), Some(0), Some(1)))
    matchState.addEvent(new Event(Some(0), Some(0), Some(0), Some(0), Some(2)))
    matchState.addEvent(new Event(Some(0), Some(0), Some(0), Some(0), Some(3)))
    matchState.addEvent(new Event(Some(0), Some(0), Some(0), Some(0), Some(4)))
    matchState.addEvent(new Event(Some(0), Some(0), Some(0), Some(0), Some(5)))


    val lastEvent = matchState.getLastEvents(2)
    lastEvent should equal (List(new Event(Some(0), Some(0), Some(0), Some(0), Some(4)), new Event(Some(0), Some(0), Some(0), Some(0), Some(5))))
  }
}
