package com.img.test

import org.junit.Test
import org.scalatest.Matchers
import org.scalatest.junit.AssertionsForJUnit

/**
 * Created by a-hjarraya on 6/21/15.
 */
class EventParserTest extends AssertionsForJUnit with Matchers {

  @Test def testWellFormedEvents() = {
    // If after 15 seconds of play, Team 1 scores 2 points, then the following will be received:
    // 0x781002 = 7868418 = 0 000000001111 00000010 00000000 0 10
    val eventInt1 = 0x781002
    val event1 = EventParser.tryParseEvent(eventInt1)
    event1 should equal(Some(new Event(Some(2), Some(0), Some(0), Some(2), Some(15))))
    // If 15 seconds later, Team 2 replies with 3 points, then the following will be received:
    // 0xf0101f = 15732767 = 0 000000011110 00000010 00000011 1 11
    val eventInt2 = 0xf0101f
    val event2 = EventParser.tryParseEvent(eventInt2)
    event2 should equal(Some(new Event(Some(3), Some(1), Some(3), Some(2), Some(30))))
  }

  @Test def readWellFormedEvents() = {
    val lines = scala.io.Source.fromURL(getClass.getResource("/sample1.txt")).getLines()
    val matchState = new MatchState()
    lines.foreach { line =>
      println(line)
      if (line.startsWith("0x"))
        EventParser.tryParseEvent(Integer.parseInt(line.drop(2), 16)).map(_.print).map(println(_))
    }
    assert(true)
  }

  @Test def readMalFormedEvents() = {
    val lines = scala.io.Source.fromURL(getClass.getResource("/sample2.txt")).getLines()
    val matchState = new MatchState()
    lines.foreach { line =>
      println(line)
      if (line.startsWith("0x"))
        EventParser.tryParseEvent(Integer.parseInt(line.drop(2), 16)).map(_.print).map(println(_))
    }
    assert(true)
  }
}
