package com.img.test

/**
 * Created by a-hjarraya on 6/21/15.
 */
case class Event(pointsScored: Option[Int], whoScored: Option[Int], team2Points: Option[Int], team1Points: Option[Int], time: Option[Long]) {
  def print: String = {
    var eventS = ""
    whoScored.map(team => eventS = eventS + " Team " + team + " Scored")
    pointsScored.map(points => eventS = eventS + " " + points + " Points")
    team1Points.map(points => eventS = eventS + " Team1: " + points)
    team2Points.map(points => eventS = eventS + " Team2: " + points)
    team2Points.map(points => eventS = eventS + " Time: " + points + " Seconds")
    eventS
  }
}

object EventParser {

  def tryParseEvent(line: Int): Option[Event] = {
    try {
      val binString = line.toBinaryString
      val pointsScored = Integer.parseInt(binString.takeRight(2), 2)
      val whoScored = Integer.parseInt(binString.takeRight(3).dropRight(2), 2)
      val team2Points = Integer.parseInt(binString.takeRight(11).dropRight(3), 2)
      val team1Points = Integer.parseInt(binString.takeRight(19).dropRight(11), 2)
      val time = Integer.parseInt(binString.dropRight(19), 2)
      Some(new Event(Some(pointsScored), Some(whoScored), Some(team2Points), Some(team1Points), Some(time)))
    } catch {
      case ex: Exception =>
        System.out.println("Not able to parse event " + line)
        None
    }
  }

}
