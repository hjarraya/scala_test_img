package com.img.test

import scala.collection.mutable

/**
 * Created by a-hjarraya on 6/21/15.
 */
class MatchState {
  //not thread safe better to use Akka 
  val events : mutable.MutableList[Event] = new mutable.MutableList[Event]()

  def getLastEvents(n:Int)={
    events.takeRight(n)
  }

  def getLastEvent : Event = {
    events.head
  }

  def getAllEvents : mutable.MutableList[Event] = {
    events.clone();
  }

  def addEvent(event:Event) = {
    events += event
  }

}
